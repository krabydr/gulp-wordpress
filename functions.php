<?php
// Recommended plugins installer
require_once 'include/plugins/init.php';
require_once('include/wpadmin/admin-addons.php');
require_once('include/cpt.php');


function style_js()
{
    wp_enqueue_script('lib', get_template_directory_uri() . '/js/lib.js', array('jquery'), '1.0', true);
    wp_enqueue_script('logic', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0', true);
    // if(customStyles()->ID == "2"){
    //     wp_enqueue_style('style-loader-home', get_template_directory_uri() . '/style/loader-home.css');
    // }else{
    //     wp_enqueue_style('style-loader-home', get_template_directory_uri() . '/style/loader-home.css');
    //     // wp_enqueue_style('style-loader', get_template_directory_uri() . '/style/loader.css');
    // }
    wp_enqueue_style('style', get_template_directory_uri() . '/style/style.css');
}

add_action('wp_enqueue_scripts', 'style_js');


add_action('wp_enqueue_scripts', 'customStyles'); 

function customStyles() {
            global $post;
         
            $params = array(
                'site_url' => site_url(),
                'admin_ajax_url' => admin_url('admin-ajax.php'),
                'post_id' => $post->ID
            );
            return get_post($params[2]);
}

// HTML5 support for IE
function wp_IEhtml5_js()
{
    global $is_IE;
    if ($is_IE)
        echo '<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><script src="//css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]--><!--[if lte IE 9]><link href="' . theme() . '/style/animations-ie-fix.css" rel="stylesheet" /><![endif]-->';
}

add_action('wp_head', 'wp_IEhtml5_js');

// Custom theme url
function theme($filepath = NULL)
{
    return preg_replace('(https?://)', '//', get_stylesheet_directory_uri() . ($filepath ? '/' . $filepath : ''));
}

//register menus
register_nav_menus(array(
    'main_menu' => 'Main menu'
));
//get images

if (!(function_exists('wp_get_attachment_by_post_name'))) {
    function wp_get_attachment_by_post_name($post_name)
    {
        $args = array(
            'posts_per_page' => 1,
            'post_type' => 'attachment',
            'name' => trim($post_name),
        );

        $get_attachment = new WP_Query($args);

        if (!$get_attachment || !isset($get_attachment->posts, $get_attachment->posts[0])) {
            return false;
        }
        $image_id = $get_attachment->post->ID;
        $image_attributes = wp_get_attachment_image_src($image_id, 'full');
        return $image_attributes[0];
    }
}


//register sidebar
$reg_sidebars = array(
    'page_sidebar' => 'Page Sidebar',
    'blog_sidebar' => 'Blog Sidebar',
    'footer_sidebar' => 'Footer Area'
);
foreach ($reg_sidebars as $id => $name) {
    register_sidebar(
        array(
            'name' => __($name),
            'id' => $id,
            'before_widget' => '<div class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3 class="widgetTitle">',
            'after_title' => '</h3>',
        )
    );
}

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
}

add_theme_support('post-thumbnails');

//images sizes
add_image_size('free', '1920', '', true);

//light function fo wp_get_attachment_image_src()
function image_src($id, $size = 'full', $background_image = false, $height = false)
{
    if ($image = wp_get_attachment_image_src($id, $size, true)) {
        return $background_image ? 'background-image: url(' . $image[0] . ');' . ($height ? 'min-height:' . $image[2] . 'px' : '') : $image[0];
    }
}

//clear wp_head

remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rel_canonical');
//remove_action('wp_head', 'qtrans_header', 10, 0);
add_action('widgets_init', 'my_remove_recent_comments_style');
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}

// Remove Emoji js/styles
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');

// Remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js($src)
{
    if (strpos($src, 'ver='))
        $src = remove_query_arg('ver', $src);
    return $src;
}

add_filter('style_loader_src', 'vc_remove_wp_ver_css_js', 9999);
add_filter('script_loader_src', 'vc_remove_wp_ver_css_js', 9999);

/* BEGIN: Theme config section*/
define('HOME_PAGE_ID', get_option('page_on_front'));
define('BLOG_ID', get_option('page_for_posts'));
define('POSTS_PER_PAGE', get_option('posts_per_page'));
/* END: Theme config section*/

//New Body Classes
function new_body_classes($classes)
{
    if (is_page()) {
        global $post;
        $temp = get_page_template();
        if ($temp != null) {
            $path = pathinfo($temp);
            $tmp = $path['filename'] . "." . $path['extension'];
            $tn = str_replace(".php", "", $tmp);
            $classes[] = $tn;
        }
        if (is_active_sidebar('sidebar')) {
            $classes[] = 'with_sidebar';
        }
        foreach ($classes as $k => $v) {
            if (
                $v == 'page-template' ||
                $v == 'page-id-' . $post->ID ||
                $v == 'page-template-default' ||
                $v == 'woocommerce-page' ||
                ($temp != null ? ($v == 'page-template-' . $tn . '-php' || $v == 'page-template-' . $tn) : '')) unset($classes[$k]);
        }
    }
    if (is_single()) {
        global $post;
        $f = get_post_format($post->ID);
        foreach ($classes as $k => $v) {
            if ($v == 'postid-' . $post->ID || $v == 'single-format-' . (!$f ? 'standard' : $f)) unset($classes[$k]);
        }
    }
    /*add browser*/
//	require_once('include/crossbrowser.php');

    return $classes;
}

add_filter('body_class', 'new_body_classes');

//remove ID in menu list
add_filter('nav_menu_item_id', 'clear_nav_menu_item_id', 10, 3);
function clear_nav_menu_item_id($id,  $item, $args)
{
    return "";
}
//Related


// Contact form 7 remove AUTOTOP
if (defined('WPCF7_VERSION')) {
    function maybe_reset_autop($form)
    {
        $form_instance = WPCF7_ContactForm::get_current();
        $manager = WPCF7_ShortcodeManager::get_instance();
        $form_meta = get_post_meta($form_instance->id(), '_form', true);
        $form = $manager->do_shortcode($form_meta);
        $form_instance->set_properties(array(
            'form' => $form
        ));
        return $form;
    }

    add_filter('wpcf7_form_elements', 'maybe_reset_autop');
}


function wp_corenavi() {
  global $wp_query;
  $pages = '';
  $max = $wp_query->max_num_pages;
  if (!$current = get_query_var('paged')) $current = 1;
  $a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
  $a['total'] = $max;
  $a['current'] = $current;

  $total = 1; //1 - выводить текст "Страница N из N", 0 - не выводить
  $a['mid_size'] = 3; //сколько ссылок показывать слева и справа от текущей
  $a['end_size'] = 1; //сколько ссылок показывать в начале и в конце
  $a['prev_text'] = '&laquo;'; //текст ссылки "Предыдущая страница"
  $a['next_text'] = '&raquo;'; //текст ссылки "Следующая страница"

  if ($max > 1) echo '<div class="navigation">';
  echo $pages . paginate_links($a);
  if ($max > 1) echo '</div>';
}

//
//$args = array(
//
//	/* (string) The title displayed on the options page. Required. */
//	'page_title' => 'Options',
//
//	/* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
//	'menu_title' => '',
//
//	/* (string) The URL slug used to uniquely identify this options page.
//	Defaults to a url friendly version of menu_title */
//	'menu_slug' => '',
//
//	/* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
//	Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
//	'capability' => 'edit_posts',
//
//	/* (int|string) The position in the menu order this menu should appear.
//	WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
//	Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
//	Defaults to bottom of utility menu items */
//	'position' => false,
//
//	/* (string) The slug of another WP admin page. if set, this will become a child page. */
//	'parent_slug' => '',
//
//	/* (string) The icon class for this menu. Defaults to default WordPress gear.
//	Read more about dashicons here: https://developer.wordpress.org/resource/dashicons/ */
//	'icon_url' => false,
//
//	/* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists).
//	If set to false, this parent page will appear alongside any child pages. Defaults to true */
//	'redirect' => true,
//
//	/* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2').
//	Defaults to 'options'. Added in v5.2.7 */
//	'post_id' => 'options',
//
//	/* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up.
//	Defaults to false. Added in v5.2.8. */
//	'autoload' => false,
//
//	/* (string) The update button text. Added in v5.3.7. */
//	'update_button'		=> __('Update', 'acf'),
//
//	/* (string) The message shown above the form on submit. Added in v5.6.0. */
//	'updated_message'	=> __("Options Updated", 'acf'),
//
//);
//acf_add_options_page( $args );