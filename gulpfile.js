var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    livereload = require('gulp-livereload'),
    autoprefixer = require('gulp-autoprefixer');


var sassFiles = 'style/scss/*.scss',
    cssDest = './style',
    jsFiles = './js/main.js',
    jsDest = './js';


gulp.task('styles', function () {
    return gulp.src([sassFiles])
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ["last 20 versions", "ie >= 9"],
            cascade: false
        }))
        .pipe(concat('style.css'))
        .pipe(gulp.dest(cssDest))
        .pipe(livereload());
});

gulp.task('lib-js', function () {
    return gulp.src(['./node_modules/jquery/dist/jquery.slim.min.js'])
        .pipe(uglify())
        .pipe(concat('lib.js'))
        .pipe(gulp.dest(jsDest));
});

gulp.task('js', function () {
    return gulp.src(jsFiles)
        .pipe(uglify())
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(livereload());
});


//
gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('./style/scss/components/*.scss', ['styles']);
    gulp.watch(jsFiles, ['js']);
});

gulp.task('default', ['styles', 'js', 'lib-js']);