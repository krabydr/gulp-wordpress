<?php get_header();?>
<section id="page-404" class=" ">
    <header>
        <a class="logo" href="<?php echo get_home_url(); ?>">
            <?php
if ($color == "white") {
    ?>
            <img src="<?php echo wp_get_attachment_by_post_name('LOGO_white1') ?>" alt="logo">
            <?php
} else {
    ?>
            <img src="<?php echo wp_get_attachment_by_post_name('logo'); ?>" alt="logo">
            <?php
}
?>
            <img class="desktop" src="<?php echo wp_get_attachment_by_post_name('logo'); ?>" alt="logo">

        </a>
    </header>
    <div class="block-404">
        <h2 class="block-404-title"><span>4</span><img
                src="<?php echo wp_get_attachment_by_post_name('pngkey.com-lion-roar-png-683682'); ?>"
                alt="Not found"><span>4</span></h2>
        <div class="goBack">
            <h2>Whoops! <span>Page is not found</span></h2>
            <a onclick="window.history.go(-1); return false;"> <span
                    data-img="<?php echo wp_get_attachment_by_post_name('arrow') ?>"></span>
                go back </a>
        </div>

    </div>
    <p>© 2018 Carrillo. All rights reserved. <a href="<?php echo site_url() ?>/terms-conditions/">Terms & Conditions</a>
    </p>

</section>
</main>
<div class="ip-container" id="ip-container">
    <!-- initial header-->
    <header class="ip-header">
        <h1 class="ip-logo"><span class="ip-inner">Title</span></h1>
        <div class="ip-loader">
            <svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">
                <path class="ip-loader-circlebg"
                    d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z">
                </path>
                <path class="ip-loader-circle" id="ip-loader-circle"
                    d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z">
                </path>
            </svg>
        </div>
    </header>
</div>
</div>
<?php wp_footer();?>
</body>

</html>