<?php get_header(); /*Template Name: terms*/?>
<section class="styles aboutUs scroll-top-block <?php echo $color ?>" id="top" data-img="<?php echo $top_image_small ?>">
    <header>
        <a class="logo" href="<?php echo get_home_url(); ?>">
            <?php
if ($color == "white") {
    ?>
            <img src="<?php echo wp_get_attachment_by_post_name('LOGO_white1') ?>" alt="logo">
            <?php
} else {
    ?>
            <img src="<?php echo wp_get_attachment_by_post_name('logo'); ?>" alt="logo">
            <?php
}
?>
            <img class="desktop" src="<?php echo wp_get_attachment_by_post_name('logo'); ?>" alt="logo">
        </a>
        <nav class="menu">
            <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location' => 'main_menu'));?>
        </nav>
        <div class="social">
        <?php get_template_part('parts/front-page/social');?>
        </div>
    </header>
</section>
<section id="terms">
    <div class="terms-inner">
        <?php
$title = get_the_title();
$title_array = explode(' ', $title);
$first_word = $title_array[0];
$lastword = $title_array[1] . " " . $title_array[2];
?>
        <h2>
            <?php echo $first_word ?> <span>
                <?php echo $lastword ?></span> </h2>
        <div>
            <?php if (have_posts()): while (have_posts()): the_post();?>
		            <?php the_content();?>
		            <?php endwhile;endif;?>
        </div>
    </div>
</section>
<?php get_footer();?>