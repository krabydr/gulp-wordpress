<?php
function cpt($post_type_name,$post_type_slug,$post_type_icon = 'dashicons-admin-page',$archive = true,$taxname = false,$taxslug = false,$tagname = false,$tagslug = false) {
	//custom taxonomy
	$taxlabels = array(
		'name'                          => $taxname,
		'singular_name'                 => $taxname,
		'search_items'                  => 'Search '.$taxname,
		'popular_items'                 => 'Popular '.$taxname,
		'all_items'                     => 'All '.$taxname.'s',
		'parent_item'                   => 'Parent '.$taxname,
		'edit_item'                     => 'Edit '.$taxname,
		'update_item'                   => 'Update '.$taxname,
		'add_new_item'                  => 'Add New '.$taxname,
		'new_item_name'                 => 'New '.$taxname,
		'separate_items_with_commas'    => 'Separate '.$taxname.'s with commas',
		'add_or_remove_items'           => 'Add or remove '.$taxname.'s',
		'choose_from_most_used'         => 'Choose from most used '.$taxname.'s'
	);

	$labels_tag = array(
		'name'                          => $tagname,
		'singular_name'                 => $tagname,
		'search_items'                  => 'Search '.$tagname.'s',
		'popular_items'                 => 'Popular '.$tagname.'s',
		'all_items'                     => 'All '.$tagname.'s',
		'parent_item'                   =>  null,
		'parent_item_colon'             =>  null,
		'edit_item'                     => 'Edit '.$tagname,
		'update_item'                   => 'Update '.$tagname,
		'add_new_item'                  => 'Add New '.$tagname,
		'new_item_name'                 => 'New '.$tagname,
		'separate_items_with_commas'    => 'Separate '.$tagname.'s with commas',
		'add_or_remove_items'           => 'Add or remove '.$tagname.'s',
		'choose_from_most_used'         => 'Choose from most used '.$tagname.'s',
		'menu_name'                     => $tagname,
	);

	$taxarr = array(
		'label'                         => $taxname,
		'labels'                        => $taxlabels,
		'public'                        => true,
		'hierarchical'                  => true,
		'show_in_nav_menus'             => true,
		'args'                          => array( 'orderby' => 'term_order' ),
		'query_var'                     => true,
		'show_ui'                       => true,
		'rewrite'                       => true,
		'show_admin_column'             => true
	);
	$tags_arr = array(
		'hierarchical' => false,
		'labels' => $labels_tag,
		'show_ui' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'rewrite' => array( 'slug' => $tagname),
	);
	/**/
	$post_type_arr = array(
		'labels' => array(
			'name' => $post_type_name,
			'singular_name' => $post_type_name,
			'menu_name' => $post_type_name
		),
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'rewrite'               => array( 'slug' => $post_type_slug ),
		'has_archive'           => $archive,
		'hierarchical'          => true,
		'show_in_nav_menus'     => true,
		'capability_type'       => 'page',
		'query_var'             => true,
		'menu_icon'             => $post_type_icon,
	);

	if ($post_type_name&&$post_type_slug&&$post_type_icon) {
		register_post_type( strtolower($post_type_slug),$post_type_arr);
	}
	if ($taxname&&$taxslug) {
		register_taxonomy( strtolower($taxslug), strtolower($post_type_slug), $taxarr );
	}
	if ($tagname&&$tagslug) {
		register_taxonomy(strtolower($tagslug),strtolower($post_type_slug),$tags_arr);
	}
}