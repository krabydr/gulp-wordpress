to<?php get_header(); /*Template Name: AboutUsSingle*/?>


<?php

$top_image = get_field('top_image');
$top_image_small = get_field('top_image_small');
$about_video = get_field('about_us_single_video');
$textBig = get_field('title_big');
$textSmall = get_field('title_small');
$textTitle = get_field('textTitle');
$textTitleDesc = get_field('textTitleDesc');
$color = get_field('color');

?>
<section class="styles aboutUs about-us-single scroll-top-block <?php echo $color ?>" id="top"
    data-img="<?php echo $top_image_small ?>">

    <header>
        <a class="logo" href="<?php echo get_home_url(); ?>">
            <?php
if ($color == "white") {
    ?>
            <img src="<?php echo wp_get_attachment_by_post_name('LOGO_white1') ?>" alt="logo">
            <?php
} else {
    ?>
            <img src="<?php echo wp_get_attachment_by_post_name('logo'); ?>" alt="logo">
            <?php
}
?>
            <img class="desktop" src="<?php echo wp_get_attachment_by_post_name('logo'); ?>" alt="logo">

        </a>
        <nav class="menu">
            <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location' => 'main_menu'));?>
        </nav>
        <div class="social">
            <a href=""> <i class="facebook" data-img="<?php echo wp_get_attachment_by_post_name('facebook') ?>"></i></a>
            <a href=""><i class="instagram"
                    data-img="<?php echo wp_get_attachment_by_post_name('instagram') ?>"></i></a>
            <a href=""><i class="twitter" data-img="<?php echo wp_get_attachment_by_post_name('twitter') ?>"></i></a>
        </div>

        <div class="prev-link">
            <a href="<?php echo get_home_url(); ?>/about-us">
                <span data-img="<?php echo wp_get_attachment_by_post_name('arrow') ?>"></span>
                About us
            </a>
        </div>
    </header>
    <div class="top-image" data-img="<?php echo $top_image ?>">
        <div class="title">
            <h4>
                <?php echo $textSmall; ?>
            </h4>
            <h2>
                <?php echo $textBig; ?>
            </h2>
        </div>
        <div class="scroll-down"><span>
                <?php the_title();?></span><a class="scroll-down-arrow" href="#services-page">
                scroll down
                <span data-img="<?php echo wp_get_attachment_by_post_name('arrow') ?>"></span></a>
        </div>
    </div>
</section>
<section id="about-us-page" class="page-block-title">
    <div class="top-block-title">
        <div class="title-header">
            <h2>
                <?php echo $textTitle; ?>
            </h2>
        </div>
        <div>
            <?php echo $textTitleDesc; ?>
        </div>
    </div>
    <div class="workExpirience">
        <div class="educate">
            <h2>Education</h2>
            <?php if (have_rows('education')):
?>
            <?php while (have_rows('education')): the_row();
    $year = get_sub_field('year');
    $description = get_sub_field('description');
    ?>
            <div class="ex-time">
                <h2><span>
                        <?php echo $year; ?></span></h2>
                <?php echo $description; ?>
            </div>
            <?php endwhile;?>
            <?php endif;?>
        </div>
        <div class="work">
            <h2>Work expirience</h2>
            <?php if (have_rows('work')):
?>
            <?php while (have_rows('work')): the_row();
    // vars
    $year = get_sub_field('year');
    $description = get_sub_field('desription');
    ?>
            <div class="ex-time">
                <h2><span>
                        <?php echo $year; ?></span></h2>
                <?php echo $description; ?>
            </div>
            <?php endwhile;?>
            <?php endif;?>
        </div>
    </div>
    <div class=" about-video">
        <div class="video-custom-controls">
            <button class="playVideo" type="button">Play</button>
            <p class="video-time">00:00:00</p>
            <button class="pauseVideo" type="button"> Pause</button>
            <button class="stopVideo" type="button">Stop</button>
        </div>
        <div class="video"><a class="video-play playVideo"></a>
            <video id="video-custom">
                <source src="<?php echo $about_video['url'] ?>" type="video/mp4">
            </video>
        </div>
        <div class="social"><a href=""><i class="facebook"
                    data-img="<?php echo wp_get_attachment_by_post_name('facebook') ?>"></i></a><a href=""><i
                    class="instagram" data-img="<?php echo wp_get_attachment_by_post_name('instagram') ?>"></i></a><a
                href=""><i class="twitter" data-img="<?php echo wp_get_attachment_by_post_name('twitter') ?>"></i></a>
        </div>
        <div class="video-placeholder">
            <a class="video-placeholder-link">
                Lorem ipsum dolor lor
            </a>
            <div class="video-placeholder-text">
                <p> PLACEHOLDER</p>
            </div>
        </div>
    </div>
    <div class="grapf">
        <h2>Lorem ipsum</h2>
        <?php if (have_rows('years')):
    $index = 0;
    $array1 = array();
    $array2 = array();
    ?>
        <div class="grapf-list">
            <div class="grapf-inner-mobile">
                <?php while (have_rows('years')): the_row();
        $year = get_sub_field('year');
        $title = get_sub_field('title');
        $description = get_sub_field('description');
        $index++;
        ?>
                <div class="grapf-inner">
                    <div class="title">
                        <span>
                            <?php echo $year; ?></span>
                        <h4>
                            <?php echo $title; ?>
                        </h4>
                    </div>
                    <div class="grapf-description">
                        <?php echo $description; ?>
                    </div>
                </div>
                <?php
    endwhile;
    ?>
            </div>
            <?php while (have_rows('years')): the_row();
        $year = get_sub_field('year');
        $title = get_sub_field('title');
        $description = get_sub_field('description');
        $index++;
        if ($index % 2 == 0) {
            array_push($array1, [$year, $title, $description]);
        } else {
            array_push($array2, [$year, $title, $description]);
        }
    endwhile;
    ?>
            <div class="grapf-inner-left">
                <?php
    for ($start1 = 0; $start1 < sizeof($array1); $start1++) {

        ?>
                <div class="grapf-inner">
                    <span>
                        <?php echo $array1[$start1][0]; ?></span>
                    <h4>
                        <?php echo $array1[$start1][1]; ?>
                    </h4>
                    <div class="grapf-description">
                        <?php echo $array1[$start1][2]; ?>
                    </div>

                </div>
                <?php

    }
    ?>
            </div>
            <div class="grapf-inner-right">
                <?php
    for ($start2 = 0; $start2 < sizeof($array2); $start2++) {

        ?>
                <div class="grapf-inner">
                    <span>
                        <?php echo $array2[$start2][0]; ?></span>
                    <h4>
                        <?php echo $array2[$start2][1]; ?>
                    </h4>
                    <div class="grapf-description">
                        <?php echo $array2[$start2][2]; ?>
                    </div>

                </div>
                <?php

    }
    ?>
            </div>
            <?php
    ?>

        </div>
        <?php endif;?>
    </div>
</section>

<div class="next-page-fotter">
    <?php get_template_part('parts/front-page/book-menu');?>
    <div class="next-article">

        <div class="prev-link">
            <a href="<?php echo get_home_url(); ?>/about-us">
                <span data-img="<?php echo wp_get_attachment_by_post_name('arrow') ?>"></span>
                About us
            </a>
        </div>
        <?php
$pagelist = get_pages("child_of=" . $post->post_parent . "&parent=" . $post->post_parent . "&sort_column=menu_order&sort_order=asc");

$pages = array();
foreach ($pagelist as $page) {
    $pages[] += $page->ID;
}
$current = array_search($post->ID, $pages);
$currentId = $post->ID;
$nextID = $pages[$current + 1];
if ($nextID === null) {
    $nextID = $pages[0];
}
if (has_post_thumbnail($currentId)):
    $image = wp_get_attachment_image_src(get_post_thumbnail_id($nextID), 'single-post-thumbnail');
endif;
$post_title = get_the_title($nextID);
$image_URI = $image[0];
?>
        <?php
if (!empty($nextID)) {?>
        <div class="next-link">
            <div class="items-link">
                <a class="read-more" href="<?php echo get_permalink($nextID); ?>"
                    title="<?php echo get_the_title($nextID); ?>">
                    <?php echo $post_title ?>
                    <span data-img="<?php echo wp_get_attachment_by_post_name('arrow') ?>"></span>
                </a>
            </div>
            <div class="img">
                <img src="<?php echo $image_URI ?>" alt="img">

            </div>
        </div>
        <?php }?>
    </div>
</div>

<?php get_footer();?>