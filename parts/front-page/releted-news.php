<div class="news-services">
    <?php
    global $post;
    $args = array('posts_per_page' => 1, 'orderby' => 'rand','category_name' => 'related' );
    $rand_posts = get_posts($args);
    foreach ($rand_posts as $post) :
        setup_postdata($post); ?>
        <div class="related-posts-after-content">
            <div class="news-block info">
                <h5>related news</h5>
                <div class="mobile-image">
                    <?php the_post_thumbnail(); ?>
                </div>
                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                <time datetime="<?php echo get_the_date('Y-m-d'); ?>"><?php echo get_the_date(get_option('date_format_custom')); ?></time>
                <p class="trim"><?php echo wp_trim_words(get_the_content(), 40); ?></p>
                <div class="items-link">
                    <a class="read-more" href="<?php the_permalink(); ?>">REad more
                        <span data-img="<?php echo wp_get_attachment_by_post_name('arrow') ?>"></span>
                    </a>
                </div>
            </div>
            <div class="image">
                <?php the_post_thumbnail(); ?>
            </div>
        </div>
    <?php endforeach;
    wp_reset_postdata(); ?>
</div>