<?php
$video = get_field('video');
$videoLinkText = get_field('videoLinkText');
$videoPlaceHolder = get_field('videoPlaceHolder');
$poster = get_field('poster');
// var_dump($video);
?>
<div class="video"><a class="video-play playVideo"></a>
<!-- <object width="420" height="315"
data="https://www.youtube.com/embed/tgbNymZ7vqY">
</object> -->

            <video id="video-custom" poster="<?php echo $poster; ?>">
                <source src="<?php echo $video['url'] ?>" type="video/mp4">
            </video>
        </div>
        <div class="social">
        <?php get_template_part('parts/front-page/social');?>
        </div>
        <div class="video-placeholder ">
            <a class="video-placeholder-link facebooks"  data-title="Carrillo" data-image="<?php echo $video['url']; ?>"
            data-url="<?php echo get_site_url(); ?>" target="_blank" title="Tweet this page"><?php echo $videoLinkText; ?>            </a>
            <div class="video-placeholder-text">
                <p> <?php echo $videoPlaceHolder; ?> </p>
            </div>
        </div>