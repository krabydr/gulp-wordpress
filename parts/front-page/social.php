<?php
$facebook = the_field("facebook", 'option');
$twitter = the_field("twitter", 'option');
$instagram = the_field("instagram", 'option');
?>

<a href="<?php echo $facebook ?>"> <i class="facebook"
        data-img="<?php echo wp_get_attachment_by_post_name('facebook') ?>"></i></a>
<a href="<?php echo $instagram ?>"> <i class="instagram"
        data-img="<?php echo wp_get_attachment_by_post_name('instagram') ?>"></i></a>
<a href="<?php echo $twitter ?>"> <i class="twitter"
        data-img="<?php echo wp_get_attachment_by_post_name('twitter') ?>"></i></a>