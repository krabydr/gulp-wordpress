<div id="bookModal" class="modal" data-open="bookModal">
    <div class="modal-content">
        <h2>Book <span>an appointment</span></h2>
        <p class="description">
            Please fill out this form and we will
            contact you as soon as possible.
        </p>
        <?php echo do_shortcode('[contact-form-7 id="370" title="Book form"]') ?>
        <div class="image-modal">
            <figure>
                <img src="<?php echo wp_get_attachment_by_post_name('clock') ?>" alt="clock">
            </figure>
        </div>
        <p>
            By clicking this button I agree to the company's
            Privacy Policy and managing my personal data.
        </p>
        <span class="close"> </span>
    </div>

</div>