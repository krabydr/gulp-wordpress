<?php
$body_title = get_field('body_title');
$body_title_small = get_field('body_title_small');
$body_description = get_field('body_description');

?>

<div class="services-single-header top-block-title">
    <div class="services-single-block title-header">
        <div class="services-single-block-inner"></div>
    </div>
    <div class="services-single-text titleFadeBox">
        <h2 class="titleFadeTitle"> <?php echo $body_title ?> <span> <?php echo $body_title_small ?></span></h2>
        <div class="titleFadeText"> <?php echo $body_description ?> </div>

    </div>
</div>