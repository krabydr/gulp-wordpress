<?php
header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));
header('Content-Type: text/html; charset=utf-8');
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header('X-UA-Compatible: IE=Edge');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php
?>
    <meta charset="UTF-8">
    <title> TITLE</title>
    <meta name="MobileOptimized" content="width" />
    <meta name="HandheldFriendly" content="True" />
    <meta http-equiv="Cache-control" content="no-cache">
    <link rel="shortcut icon" href="" type="image/x-icon" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <?php wp_head();?>
</head>
<body <?php body_class();?>>
    <div id="container">
        <div id="content" role="main">
            <main id="main">
    